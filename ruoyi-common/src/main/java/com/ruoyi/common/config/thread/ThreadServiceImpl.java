package com.ruoyi.common.config.thread;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * 多线程执行逻辑
 *
 * @author xincai
 */
@Service
public class ThreadServiceImpl {

    @Autowired
    private ThreadPoolConfig threadPool;

    public void threadAddList() {
        CountDownLatch countXwqzZb = new CountDownLatch(4);
        saveHZjbgXwqzhz(new Object(), new ArrayList<>(), countXwqzZb);
        try {
            //主线程等待4个线程入库 , 执行完毕时在执行
            countXwqzZb.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 批处理入库
     *
     * @param objMapper      mapper对象
     * @param listDto        入库的list
     * @param countDownLatch 同步等待(如不需要则不加)
     */
    public void saveHZjbgXwqzhz(Object objMapper, List<String> listDto, CountDownLatch countDownLatch) {
        //根据配置线程核心数量(4)
        int corePoolSize = threadPool.getCorePoolSize();
        int count = listDto.size() / corePoolSize;
        List<String> newlist = null;
        //分4个线程执行
        for (int i = 0; i < corePoolSize; i++) {
            int startIndex = (i * count);
            int endIndex = (i + 1) * count;
            if (i == corePoolSize - 1) {
                endIndex = listDto.size();
            }
            newlist = listDto.subList(startIndex, endIndex);
            List<String> finalNewlist = newlist;
            if (finalNewlist.size() == 0) {
                return;
            }
            System.out.println("线程" + i + "新增行为权重数据" + startIndex + "到" + endIndex);
            threadPool.threadPoolTaskExecutor().execute(() -> {
                try {
                    //调用数据库实现插入finalNewlist
                    // hZjbgXwqzhzMapper.addHZjbgXwqzhzEntityList(finalNewlist);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
    }
}
