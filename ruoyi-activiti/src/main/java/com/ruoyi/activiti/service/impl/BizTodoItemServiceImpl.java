package com.ruoyi.activiti.service.impl;

import com.ruoyi.activiti.domain.BizTodoItem;
import com.ruoyi.activiti.mapper.BizTodoItemMapper;
import com.ruoyi.activiti.service.IBizTodoItemService;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 待办事项Service业务层处理
 *
 * @author Xianlu Tech
 * @date 2019-11-08
 */
@Service
@Transactional
public class BizTodoItemServiceImpl implements IBizTodoItemService {
    private static final Logger logger = LoggerFactory.getLogger(BizTodoItemServiceImpl.class);
    @Autowired
    private BizTodoItemMapper bizTodoItemMapper;
    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private TaskService taskService;

    /**
     * 查询待办事项
     *
     * @param id 待办事项ID
     * @return 待办事项
     */
    @Override
    public BizTodoItem selectBizTodoItemById(Long id) {
        return bizTodoItemMapper.selectBizTodoItemById(id);
    }

    /**
     * 查询待办事项列表
     *
     * @param bizTodoItem 待办事项
     * @return 待办事项
     */
    @Override
    public List<BizTodoItem> selectBizTodoItemList(BizTodoItem bizTodoItem) {
        return bizTodoItemMapper.selectBizTodoItemList(bizTodoItem);
    }

    /**
     * 新增待办事项
     *
     * @param bizTodoItem 待办事项
     * @return 结果
     */
    @Override
    public int insertBizTodoItem(BizTodoItem bizTodoItem) {
        return bizTodoItemMapper.insertBizTodoItem(bizTodoItem);
    }

    /**
     * 修改待办事项
     *
     * @param bizTodoItem 待办事项
     * @return 结果
     */
    @Override
    public int updateBizTodoItem(BizTodoItem bizTodoItem) {
        return bizTodoItemMapper.updateBizTodoItem(bizTodoItem);
    }

    /**
     * 删除待办事项对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBizTodoItemByIds(String ids) {
        return bizTodoItemMapper.deleteBizTodoItemByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除待办事项信息
     *
     * @param id 待办事项ID
     * @return 结果
     */
    @Override
    public int deleteBizTodoItemById(Long id) {
        return bizTodoItemMapper.deleteBizTodoItemById(id);
    }

    /**
     * 下一节点处理人待办事项
     *
     * @param instanceId  流程实例id
     * @param itemName    请求标题
     * @param itemContent 请求原因
     * @param module      流程key
     * @return 任务个数
     */
    @Override
    public int insertTodoItem(String instanceId, String itemName, String itemContent, String module) {
        //创建代办对象-入库
        BizTodoItem todoItem = new BizTodoItem();
        todoItem.setItemName(itemName);
        todoItem.setItemContent(itemContent);
        todoItem.setIsView("0");
        todoItem.setIsHandle("0");
        todoItem.setModule(module);
        todoItem.setTodoTime(DateUtils.getNowDate());
        //任务管理服务.获取流程实例id的所有任务
        List<Task> taskList = taskService.createTaskQuery().processInstanceId(instanceId).active().list();
        int counter = 0;
        for (Task task : taskList) {
            logger.info("根据流程实例id查询到task任务 : {}", task);
            // todoitem 去重
            BizTodoItem bizTodoItem = bizTodoItemMapper.selectTodoItemByTaskId(task.getId());
            if (bizTodoItem != null) {
                continue;
            }
            BizTodoItem newItem = new BizTodoItem();
            BeanUtils.copyProperties(todoItem, newItem);
            //流程id
            newItem.setInstanceId(instanceId);
            //任务id
            newItem.setTaskId(task.getId());
            //任务名称
            newItem.setTaskName("task:" + task.getTaskDefinitionKey().substring(0, 1).toUpperCase() + task.getTaskDefinitionKey().substring(1));
            newItem.setNodeName(task.getName());
            //获取下一个办理人
            String assignee = task.getAssignee();
            logger.info("task id={},name={} ,代办人 : {}", task.getId(), task.getName(), assignee);
            if (StringUtils.isNotBlank(assignee)) {
                newItem.setTodoUserId(assignee);
                SysUser user = userMapper.selectUserByLoginName(assignee);
                newItem.setTodoUserName(user.getUserName());
                bizTodoItemMapper.insertBizTodoItem(newItem);
                counter++;
            } else {
                // 查询候选用户组
                List<String> todoUserIdList = bizTodoItemMapper.selectTodoUserListByTaskId(task.getId());
                logger.info("查询task任务id={},的候选用户组{}", task.getId(), todoUserIdList.toString());
                if (!CollectionUtils.isEmpty(todoUserIdList)) {
                    for (String todoUserId : todoUserIdList) {
                        SysUser todoUser = userMapper.selectUserByLoginName(todoUserId);
                        newItem.setTodoUserId(todoUser.getLoginName());
                        newItem.setTodoUserName(todoUser.getUserName());
                        bizTodoItemMapper.insertBizTodoItem(newItem);
                        counter++;
                    }
                } else {
                    // 查询候选用户(动态可能多个)
                    List<String> todoUsers = bizTodoItemMapper.selectTodoUserByTaskId(task.getId());
                    if (!CollectionUtils.isEmpty(todoUsers)) {
                        for (String todoUserId : todoUsers) {
                            SysUser todoUser = userMapper.selectUserByLoginName(todoUserId);
                            newItem.setTodoUserId(todoUser.getLoginName());
                            newItem.setTodoUserName(todoUser.getUserName());
                            bizTodoItemMapper.insertBizTodoItem(newItem);
                            counter++;
                        }
                    }
                }
            }
        }
        return counter;
    }

    @Override
    public BizTodoItem selectBizTodoItemByCondition(String taskId, String todoUserId) {
        return bizTodoItemMapper.selectTodoItemByCondition(taskId, todoUserId);
    }
}
