package com.ruoyi.web.controller.ruoyi;

import com.ruoyi.common.utils.RedisUtil;
import com.ruoyi.system.domain.SysDept;
import com.ruoyi.system.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * test
 *
 * @author xincai
 */
@RestController
@RequestMapping("ruoyi")
public class RuoyiController {
    @Autowired
    private RedisUtil redisUtil;

    /**
     *
     */
    @GetMapping("/test")
    public void test() {
        List<String> list = new ArrayList<>();
        List<SysDept> userList = new ArrayList<SysDept>();
        SysDept u=new SysDept();
        u.setAncestors("131312");
        u.setEmail("www.gaode.com");
        u.setDeptName("领导部门");
        userList.add(u);
        userList.add(u);
        list.add("上次看什么材料");
        list.add("1321313");
        redisUtil.set("caixin", userList);
        List<SysUser> sysUsers= (List<SysUser>) redisUtil.get("caixin");
        System.out.println(sysUsers);
    }
}
